# Korean translation for desktop-icons.
# Copyright (C) 2021 desktop-icons's COPYRIGHT HOLDER
# This file is distributed under the same license as the desktop-icons package.
# JGJeong <jingimem@gmail.com>, 2021.
# Junghee Lee <daemul72@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: desktop-icons master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-11 23:01+0100\n"
"PO-Revision-Date: 2022-04-23 10:08+0900\n"
"Last-Translator: 이정희 <daemul72@gmail.com>\n"
"Language-Team: Korean <jingimem@gmail.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.0.1\n"

#: ../app/askRenamePopup.js:47
msgid "Folder name"
msgstr "폴더 이름"

#: ../app/askRenamePopup.js:47
msgid "File name"
msgstr "파일 이름"

#: ../app/askRenamePopup.js:55 ../app/autoAr.js:305
#: ../app/desktopManager.js:1000
msgid "OK"
msgstr "확인"

#: ../app/askRenamePopup.js:55
msgid "Rename"
msgstr "이름 바꾸기"

#: ../app/autoAr.js:88
msgid "AutoAr is not installed"
msgstr ""

#: ../app/autoAr.js:89
msgid ""
"To be able to work with compressed files, install file-roller and/or gir-1.2-"
"gnomeAutoAr"
msgstr ""

#: ../app/autoAr.js:224
#, fuzzy
msgid "Extracting files"
msgstr "여기 풀기"

#: ../app/autoAr.js:241
#, fuzzy
msgid "Compressing files"
msgstr "{0}개 파일 압축하기"

#: ../app/autoAr.js:297 ../app/autoAr.js:636 ../app/desktopManager.js:1002
#: ../app/fileItemMenu.js:461
msgid "Cancel"
msgstr "취소"

#: ../app/autoAr.js:318 ../app/autoAr.js:619
msgid "Enter a password here"
msgstr ""

#: ../app/autoAr.js:359
msgid "Removing partial file '${outputFile}'"
msgstr ""

#: ../app/autoAr.js:378
msgid "Creating destination folder"
msgstr ""

#: ../app/autoAr.js:410
msgid "Extracting files into '${outputPath}'"
msgstr ""

#: ../app/autoAr.js:442
#, fuzzy
msgid "Extraction completed"
msgstr "여기 풀기"

#: ../app/autoAr.js:443
msgid "Extracting '${fullPathFile}' has been completed."
msgstr ""

#: ../app/autoAr.js:449
#, fuzzy
msgid "Extraction cancelled"
msgstr "여기 풀기"

#: ../app/autoAr.js:450
msgid "Extracting '${fullPathFile}' has been cancelled by the user."
msgstr ""

#: ../app/autoAr.js:460
msgid "Passphrase required for ${filename}"
msgstr ""

#: ../app/autoAr.js:463
msgid "Error during extraction"
msgstr ""

#: ../app/autoAr.js:492
msgid "Compressing files into '${outputFile}'"
msgstr ""

#: ../app/autoAr.js:505
msgid "Compression completed"
msgstr ""

#: ../app/autoAr.js:506
msgid "Compressing files into '${outputFile}' has been completed."
msgstr ""

#: ../app/autoAr.js:510 ../app/autoAr.js:517
msgid "Cancelled compression"
msgstr ""

#: ../app/autoAr.js:511
msgid "The output file '${outputFile}' already exists."
msgstr ""

#: ../app/autoAr.js:518
msgid "Compressing files into '${outputFile}' has been cancelled by the user."
msgstr ""

#: ../app/autoAr.js:521
msgid "Error during compression"
msgstr ""

#: ../app/autoAr.js:554
msgid "Create archive"
msgstr ""

#: ../app/autoAr.js:579
#, fuzzy
msgid "Archive name"
msgstr "파일 이름"

#: ../app/autoAr.js:614
msgid "Password"
msgstr ""

#: ../app/autoAr.js:633
msgid "Create"
msgstr ""

#: ../app/autoAr.js:712
msgid "Compatible with all operating systems."
msgstr ""

#: ../app/autoAr.js:718
msgid "Password protected .zip, must be installed on Windows and Mac."
msgstr ""

#: ../app/autoAr.js:724
msgid "Smaller archives but Linux and Mac only."
msgstr ""

#: ../app/autoAr.js:730
msgid "Smaller archives but must be installed on Windows and Mac."
msgstr ""

#: ../app/dbusUtils.js:69
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr "\"${programName}\"은(는) 바탕 화면 아이콘에 필요합니다"

#: ../app/dbusUtils.js:70
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""
"이 기능이 바탕 화면 아이콘에서 작동하려면 시스템에 \"${programName}\"을(를) "
"설치해야 합니다."

#: ../app/desktopGrid.js:53
#, fuzzy
msgid "Desktop icons"
msgstr "바탕화면 아이콘 설정"

#: ../app/desktopIconsUtil.js:154
msgid "Command not found"
msgstr "명령을 찾을 수 없습니다"

#: ../app/desktopManager.js:258
msgid "Nautilus File Manager not found"
msgstr "노틸러스 파일 관리자를 찾을 수 없습니다"

#: ../app/desktopManager.js:259
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr "노틸러스 파일 관리자는 Desktop Icons NG로 작업하는 데 필수입니다."

#: ../app/desktopManager.js:962
msgid "Clear Current Selection before New Search"
msgstr "새 검색 전에 현재 선택 지우기"

#: ../app/desktopManager.js:1004
msgid "Find Files on Desktop"
msgstr "데스크탑에서 파일 찾기"

#: ../app/desktopManager.js:1070 ../app/desktopManager.js:1773
msgid "New Folder"
msgstr "새 폴더"

#: ../app/desktopManager.js:1074
msgid "New Document"
msgstr "새 문서"

#: ../app/desktopManager.js:1079
msgid "Paste"
msgstr "붙여넣기"

#: ../app/desktopManager.js:1083
msgid "Undo"
msgstr "실행 취소"

#: ../app/desktopManager.js:1087
msgid "Redo"
msgstr "다시 실행"

#: ../app/desktopManager.js:1093
msgid "Select All"
msgstr "모두 선택"

#: ../app/desktopManager.js:1101
msgid "Show Desktop in Files"
msgstr "파일에 바탕화면 표시"

#: ../app/desktopManager.js:1105 ../app/fileItemMenu.js:356
msgid "Open in Terminal"
msgstr "터미널로 열기"

#: ../app/desktopManager.js:1111
msgid "Change Background…"
msgstr "배경화면 변경…"

#: ../app/desktopManager.js:1122
msgid "Desktop Icons Settings"
msgstr "바탕화면 아이콘 설정"

#: ../app/desktopManager.js:1126
msgid "Display Settings"
msgstr "화면 설정"

#: ../app/desktopManager.js:1786
#, fuzzy
msgid "Folder Creation Failed"
msgstr "폴더 이름"

#: ../app/desktopManager.js:1787
#, fuzzy
msgid "Error while trying to create a Folder"
msgstr "파일 삭제 중 오류가 발생하였습니다."

#: ../app/desktopManager.js:1823
msgid "Template Creation Failed"
msgstr ""

#: ../app/desktopManager.js:1824
msgid "Error while trying to create a Document"
msgstr ""

#: ../app/desktopManager.js:1832
msgid "Arrange Icons"
msgstr "아이콘 정렬"

#: ../app/desktopManager.js:1836
msgid "Arrange By..."
msgstr "정렬 기준..."

#: ../app/desktopManager.js:1845
msgid "Keep Arranged..."
msgstr "정렬 유지..."

#: ../app/desktopManager.js:1849
msgid "Keep Stacked by type..."
msgstr "유형별로 스택됨 유지..."

#: ../app/desktopManager.js:1854
msgid "Sort Home/Drives/Trash..."
msgstr "집/드라이브/휴지통 정렬..."

#: ../app/desktopManager.js:1860
msgid "Sort by Name"
msgstr "이름순으로 정렬"

#: ../app/desktopManager.js:1862
msgid "Sort by Name Descending"
msgstr "이름 내림차순으로 정렬"

#: ../app/desktopManager.js:1865
msgid "Sort by Modified Time"
msgstr "수정된 시간순으로 정렬"

#: ../app/desktopManager.js:1868
msgid "Sort by Type"
msgstr "유형순으로 정렬"

#: ../app/desktopManager.js:1871
msgid "Sort by Size"
msgstr "크기순으로 정렬"

#. * TRANSLATORS: when using a screen reader, this is the text read when a folder is
#. selected. Example: if a folder named "things" is selected, it will say "things Folder"
#: ../app/fileItem.js:63
msgid "${VisibleName} Folder"
msgstr ""

#. * TRANSLATORS: when using a screen reader, this is the text read when a normal file is
#. selected. Example: if a file named "my_picture.jpg" is selected, it will say "my_picture.jpg File"
#: ../app/fileItem.js:67
msgid "${VisibleName} File"
msgstr ""

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: ../app/fileItem.js:71 ../app/fileItem.js:201
msgid "Home"
msgstr "홈"

#. * TRANSLATORS: when using a screen reader, this is the text read when the trash folder is
#. selected.
#: ../app/fileItem.js:76
#, fuzzy
msgid "Trash"
msgstr "휴지통 비우기"

#. * TRANSLATORS: when using a screen reader, this is the text read when an external drive is
#. selected. Example: if a USB stick named "my_portable" is selected, it will say "my_portable Drive"
#: ../app/fileItem.js:81
msgid "${VisibleName} Drive"
msgstr ""

#. * TRANSLATORS: when using a screen reader, this is the text read when a stack is
#. selected. Example: if a stack named "pictures" is selected, it will say "pictures Stack"
#: ../app/fileItem.js:86
msgid "${VisibleName} Stack"
msgstr ""

#: ../app/fileItem.js:293
#, fuzzy
msgid "Error while reading Desktop file"
msgstr "파일 삭제 중 오류가 발생하였습니다."

#: ../app/fileItem.js:335
msgid "Broken Link"
msgstr "깨진 링크"

#: ../app/fileItem.js:336
msgid "Can not open this File because it is a Broken Symlink"
msgstr "이 파일은 손상된 심볼릭링크이므로 열 수 없습니다"

#: ../app/fileItem.js:368
#, fuzzy
msgid "Can't open the file"
msgstr "파일 실행"

#: ../app/fileItem.js:394
msgid "Broken Desktop File"
msgstr "깨진 바탕화면 파일"

#: ../app/fileItem.js:395
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"\t<b>Edit the file to set the correct executable Program.</b>"
msgstr ""
"이 .desktop 파일에 오류가 있거나 권한이 없는 프로그램이 있습니다. 실행할 수 "
"없습니다.\n"
"\n"
"<b>파일을 편집하여 올바른 실행 프로그램을 설정합니다.</b>"

#: ../app/fileItem.js:401
msgid "Invalid Permissions on Desktop File"
msgstr "바탕화면 파일에 대한 잘못된 권한"

#: ../app/fileItem.js:402
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""
"이 .desktop 파일에 잘못된 권한이 있습니다. 마우스 오른쪽 버튼을 클릭하여 속성"
"을 편집 한 다음:\n"

#: ../app/fileItem.js:404
msgid ""
"\n"
"<b>Set Permissions, in \"Others Access\", \"Read Only\" or \"None\"</b>"
msgstr ""
"\n"
"<b>\"다른 접근\", \"읽기 전용\" 또는 \"없음\"에서 권한 설정</b>"

#: ../app/fileItem.js:407
msgid ""
"\n"
"<b>Enable option, \"Allow Executing File as a Program\"</b>"
msgstr ""
"\n"
"<b>활성화 옵션, \"파일을 프로그램으로 실행하기 허용\"</b>"

#: ../app/fileItem.js:415
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"<b>Enable \"Allow Launching\"</b>"
msgstr ""
"이 .desktop 파일은 신뢰할 수 없으며 실행할 수 없습니다. 시작을 활성화하려면 "
"마우스 오른쪽 버튼을 클릭하고 다음을 수행합니다.\n"
"\n"
"<b>\"시작하기 허용\" 활성화</b>"

#: ../app/fileItem.js:618
msgid "Failed to set execution flag"
msgstr ""

#: ../app/fileItem.js:630 ../app/fileItem.js:637
msgid "Could not apply discrete GPU environment"
msgstr ""

#: ../app/fileItem.js:666
msgid "Could not find discrete GPU data"
msgstr ""

#: ../app/fileItem.js:791
msgid "Failed to set metadata::trusted flag"
msgstr ""

#: ../app/fileItemMenu.js:135
msgid "Open All..."
msgstr "모두 열기…"

#: ../app/fileItemMenu.js:135
msgid "Open"
msgstr "열기"

#: ../app/fileItemMenu.js:151
msgid "Stack This Type"
msgstr "이 유형 스택"

#: ../app/fileItemMenu.js:151
msgid "Unstack This Type"
msgstr "이 유형 스택 해제"

#: ../app/fileItemMenu.js:164
msgid "Scripts"
msgstr "스크립트"

#: ../app/fileItemMenu.js:169
msgid "Open All With Other Application..."
msgstr "다른 프로그램으로 모두 열기…"

#: ../app/fileItemMenu.js:169
msgid "Open With Other Application"
msgstr "다른 프로그램으로 열기"

#: ../app/fileItemMenu.js:175
msgid "Launch using Dedicated Graphics Card"
msgstr "전용 그래픽 카드를 사용하여 실행"

#: ../app/fileItemMenu.js:186
msgid "Run as a program"
msgstr "프로그램으로 실행"

#: ../app/fileItemMenu.js:194
msgid "Cut"
msgstr "잘라내기"

#: ../app/fileItemMenu.js:201
msgid "Copy"
msgstr "복사하기"

#: ../app/fileItemMenu.js:209
msgid "Rename…"
msgstr "이름 바꾸기…"

#: ../app/fileItemMenu.js:219
msgid "Move to Trash"
msgstr "휴지통으로 옮기기"

#: ../app/fileItemMenu.js:227
msgid "Delete permanently"
msgstr "영구 삭제"

#: ../app/fileItemMenu.js:237
msgid "Don't Allow Launching"
msgstr "실행을 허용하지 않음"

#: ../app/fileItemMenu.js:237
msgid "Allow Launching"
msgstr "실행 허용"

#: ../app/fileItemMenu.js:250
msgid "Empty Trash"
msgstr "휴지통 비우기"

#: ../app/fileItemMenu.js:263
msgid "Eject"
msgstr "꺼내기"

#: ../app/fileItemMenu.js:271
msgid "Unmount"
msgstr "마운트 해제"

#: ../app/fileItemMenu.js:285 ../app/fileItemMenu.js:292
msgid "Extract Here"
msgstr "여기 풀기"

#: ../app/fileItemMenu.js:299
msgid "Extract To..."
msgstr "다른 위치에 풀기…"

#: ../app/fileItemMenu.js:308
msgid "Send to..."
msgstr "다음으로 보내기…"

#: ../app/fileItemMenu.js:316
#, fuzzy
msgid "Compress {0} folder"
msgid_plural "Compress {0} folders"
msgstr[0] "{0}개 파일 압축하기"

#: ../app/fileItemMenu.js:323
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "{0}개 파일 압축하기"

#: ../app/fileItemMenu.js:331
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "{0}개 파일 새 폴더로 이동"

#: ../app/fileItemMenu.js:342
msgid "Common Properties"
msgstr "공통 속성"

#: ../app/fileItemMenu.js:342
msgid "Properties"
msgstr "속성"

#: ../app/fileItemMenu.js:349
msgid "Show All in Files"
msgstr "파일에 모두 표시"

#: ../app/fileItemMenu.js:349
msgid "Show in Files"
msgstr "파일로 표시"

#: ../app/fileItemMenu.js:436
#, fuzzy
msgid "No Extraction Folder"
msgstr "여기 풀기"

#: ../app/fileItemMenu.js:437
msgid "Unable to extract File, extraction Folder Does not Exist"
msgstr ""

#: ../app/fileItemMenu.js:457
msgid "Select Extract Destination"
msgstr "대상 압축풀기 선택"

#: ../app/fileItemMenu.js:462
msgid "Select"
msgstr "선택"

#: ../app/fileItemMenu.js:503
msgid "Can not email a Directory"
msgstr "디렉터리는 이메일로 보낼 수 없습니다"

#: ../app/fileItemMenu.js:504
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr "선택 파일 중 디렉터리가 포함되어 있어, 먼저 파일로 압축이 필요합니다."

#: ../app/notifyX11UnderWayland.js:37
msgid "Desktop Icons NG is running under X11Wayland"
msgstr ""

#: ../app/notifyX11UnderWayland.js:38
msgid ""
"It seems that you have your system configured to force GTK to use X11. This "
"works, but it's suboptimal. You should check your system configuration to "
"fix this."
msgstr ""

#: ../app/notifyX11UnderWayland.js:39 ../app/showErrorPopup.js:39
msgid "Close"
msgstr "닫기"

#: ../app/notifyX11UnderWayland.js:47
msgid "Don't show this message anymore."
msgstr ""

#: ../app/preferences.js:91
msgid "Settings"
msgstr "설정"

#: ../app/prefswindow.js:64
msgid "Size for the desktop icons"
msgstr "바탕화면 아이콘 크기"

#: ../app/prefswindow.js:64
msgid "Tiny"
msgstr "매우 작은 아이콘"

#: ../app/prefswindow.js:64
msgid "Small"
msgstr "작은 아이콘"

#: ../app/prefswindow.js:64
msgid "Standard"
msgstr "중간 아이콘"

#: ../app/prefswindow.js:64
msgid "Large"
msgstr "큰 아이콘"

#: ../app/prefswindow.js:65
msgid "Show the personal folder in the desktop"
msgstr "바탕화면에 개인 폴더 표시"

#: ../app/prefswindow.js:66
msgid "Show the trash icon in the desktop"
msgstr "바탕화면에 휴지통 표시"

#: ../app/prefswindow.js:67
#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:9
msgid "Show external drives in the desktop"
msgstr "바탕화면에 외장 드라이브 표시"

#: ../app/prefswindow.js:68
#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:11
msgid "Show network drives in the desktop"
msgstr "바탕화면에 네트워크 드라이브 표시"

#: ../app/prefswindow.js:71
msgid "New icons alignment"
msgstr "새 아이콘 정렬 방식"

#: ../app/prefswindow.js:73
msgid "Top-left corner"
msgstr "좌측 상단부터"

#: ../app/prefswindow.js:74
msgid "Top-right corner"
msgstr "우측 상단부터"

#: ../app/prefswindow.js:75
msgid "Bottom-left corner"
msgstr "좌측 하단부터"

#: ../app/prefswindow.js:76
msgid "Bottom-right corner"
msgstr "우측 하단부터"

#: ../app/prefswindow.js:78
#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:13
msgid "Add new drives to the opposite side of the screen"
msgstr "새 드라이브를 스크린 반대편에 추가"

#: ../app/prefswindow.js:79
msgid "Highlight the drop place during Drag'n'Drop"
msgstr "드래그 앤 드랍 시 드랍 위치 강조 효과"

#: ../app/prefswindow.js:80
#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:27
msgid "Use Nemo to open folders"
msgstr "폴더를 여는 Nemo 사용"

#: ../app/prefswindow.js:82
msgid "Add an emblem to soft links"
msgstr "소프트 링크에 엠블럼 추가"

#: ../app/prefswindow.js:84
msgid "Use dark text in icon labels"
msgstr "아이콘 레이블에 어두운 텍스트 사용"

#: ../app/prefswindow.js:91
msgid "Settings shared with Nautilus"
msgstr "노틸러스와 설정 공유"

#: ../app/prefswindow.js:113
msgid "Click type for open files"
msgstr "파일 열기 시 클릭 유형"

#: ../app/prefswindow.js:113
msgid "Single click"
msgstr "한 번 클릭"

#: ../app/prefswindow.js:113
msgid "Double click"
msgstr "두 번 클릭"

#: ../app/prefswindow.js:114
msgid "Show hidden files"
msgstr "숨김 파일 표시"

#: ../app/prefswindow.js:115
msgid "Show a context menu item to delete permanently"
msgstr "영구적으로 삭제할 컨텍스트 메뉴 항목 표시"

#: ../app/prefswindow.js:120
msgid "Action to do when launching a program from the desktop"
msgstr "바탕화면에서 프로그램을 실행할 때 수행할 작업"

#: ../app/prefswindow.js:121
msgid "Display the content of the file"
msgstr "파일의 내용 표시"

#: ../app/prefswindow.js:122
msgid "Launch the file"
msgstr "파일 실행"

#: ../app/prefswindow.js:123
msgid "Ask what to do"
msgstr "어떤 작업 수행할 지 선택"

#: ../app/prefswindow.js:129
msgid "Show image thumbnails"
msgstr "이미지 마중그림 표시"

#: ../app/prefswindow.js:130
msgid "Never"
msgstr "보지 않음"

#: ../app/prefswindow.js:131
msgid "Local files only"
msgstr "로컬 파일만"

#: ../app/prefswindow.js:132
msgid "Always"
msgstr "항상"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:1
msgid "Icon size"
msgstr "아이콘 크기"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:2
msgid "Set the size for the desktop icons."
msgstr "바탕 화면 아이콘의 크기를 지정합니다."

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:3
msgid "Show personal folder"
msgstr "개인 폴더 표시"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:4
msgid "Show the personal folder in the desktop."
msgstr "바탕화면에 개인 폴더를 표시합니다."

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:5
msgid "Show trash icon"
msgstr "휴지통 아이콘 표시"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:6
msgid "Show the trash icon in the desktop."
msgstr "바탕화면에 휴지통 아이콘을 표시합니다."

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:7
msgid "New icons start corner"
msgstr "새 아이콘 배치할 코너"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:8
msgid "Set the corner from where the icons will start to be placed."
msgstr "아이콘 배치를 시작할 모서리를 설정합니다."

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:10
msgid "Show the disk drives connected to the computer."
msgstr "컴퓨터에 연결된 디스크 드라이브를 표시합니다."

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:12
msgid "Show mounted network volumes in the desktop."
msgstr "데스크탑에 탑재된 네트워크 볼륨을 표시합니다."

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:14
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr "바탕화면에 드라이브 및 볼륨을 추가할 때 화면의 반대쪽에 추가합니다."

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:15
msgid "Shows a rectangle in the destination place during DnD"
msgstr "끌어서 놓는 동안 대상 위치에 사격형 표시"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:16
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""
"끌어서 놓기 작업을 수행할 때, 격자에서 아이콘이 배치될 위치를 반투명 사각형"
"을 표시합니다."

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:17
msgid "Sort Special Folders - Home/Trash Drives."
msgstr "특수 폴더 정렬 - 홈/휴지통 드라이브 입니다."

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:18
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""
"바탕화면에 아이콘을 정렬할 때 홈, 휴지통 및 마운트된 네트워크 또는 외장 드라"
"이브의 위치를 정렬하고 변경합니다"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:19
msgid "Keep Icons Arranged"
msgstr "아이콘 정렬 유지"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:20
msgid "Always keep Icons Arranged by the last arranged order"
msgstr "항상 마지막 정렬 순서로 아이콘 정렬 유지"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:21
msgid "Arrange Order"
msgstr "순서 정렬"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:22
msgid "Icons Arranged by this property"
msgstr "이 속성으로 정렬된 아이콘"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:23
msgid "Keep Icons Stacked"
msgstr "아이콘 스택됨 유지"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:24
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr "항상 아이콘 스택됨 유지, 유사한 유형이 그룹화됩니다"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:25
msgid "Type of Files to not Stack"
msgstr "스택하지 않을 파일 유형"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:26
msgid "An Array of strings types, Don't Stack these types of files"
msgstr "문자열 유형의 배열, 이러한 유형의 파일을 스택하지 않습니다"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:28
msgid "Use Nemo instead of Nautilus to open folders."
msgstr "노틸러스 대신 Nemo를 사용하여 폴더를 엽니다."

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:29
msgid "Add an emblem to links"
msgstr "링크에 엠블럼 추가"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:30
msgid "Add an emblem to allow to identify soft links."
msgstr "소프트 링크를 식별할 수 있도록 엠블럼을 추가합니다."

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:31
msgid "Use black for label text"
msgstr "레이블 텍스트에 검정색 사용"

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:32
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""
"레이블 텍스트를 흰색 대신 검정색으로 칠합니다. 밝은 배경을 사용할 때 유용합니"
"다."

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:33
msgid "Show a popup if running on X11Wayland"
msgstr ""

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:34
msgid ""
"Whether DING should show a popup if it is running on X11Wayland, or the user "
"decided to not show it anymore."
msgstr ""

#~ msgid ""
#~ "To configure Desktop Icons NG, do right-click in the desktop and choose "
#~ "the last item: 'Desktop Icons settings'"
#~ msgstr ""
#~ "Desktop Icons NG 설정을 보려면 바탕화면에서 우클릭하여 마지막 아이템 '바탕"
#~ "화면 아이콘 설정'을 선택하세요."

#~ msgid "Do you want to run “{0}”, or display its contents?"
#~ msgstr "“{0}”을 실행하시겠습니까? 혹은 내용을 표시하겠습니까?"

#~ msgid "“{0}” is an executable text file."
#~ msgstr "“{0}”는 실행가능한 텍스트 파일입니다."

#~ msgid "Execute in a terminal"
#~ msgstr "터미널로 실행"

#~ msgid "Show"
#~ msgstr "보기"

#~ msgid "Execute"
#~ msgstr "실행"

#~ msgid "New folder"
#~ msgstr "새 폴더"

#~ msgid "Delete"
#~ msgstr "삭제"

#~ msgid "Are you sure you want to permanently delete these items?"
#~ msgstr "파일들을 영구 삭제하시겠습니까?"

#~ msgid "If you delete an item, it will be permanently lost."
#~ msgstr "파일을 삭제하면, 영구적으로 잃게 될 것입니다."
